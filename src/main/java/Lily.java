public class Lily extends Flower{
    private final double price = 13.35;

    public Lily(String name, String color, double length) {
        super(name, color, length);
    }

    public double getPrice() {
        return price;
    }

    public void smell(){
        System.out.println(" This flower smell as a lily.");
    }
}
