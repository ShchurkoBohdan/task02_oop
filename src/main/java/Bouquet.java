import java.util.ArrayList;

public class Bouquet {
    private String name;
    private ArrayList<Flower> flowers;
    private double price = 0;
    private int qty = 0;

    public Bouquet(String name) {
        this.name = name;
        this.flowers = new ArrayList<Flower>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public boolean addFlower (Flower flower){
        this.flowers.add(flower);
        this.qty = this.getQty() + 1;
        this.price = this.getPrice() + flower.getPrice();
        return true;
    }

    @Override
    public String toString() {
        System.out.println("This is bouquet " + getName() + " that contains " + getQty() + " flowers. It costs " + getPrice() + " dollars.");
        for (Flower i : flowers){
            System.out.println("Flower - " + i.getName() + ", with price " + i.getPrice());
        }
        return "";
    }
}
