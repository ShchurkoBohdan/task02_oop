public class Main {
    public static void main(String[] args) {
        Bouquet bouquet1 = new Bouquet("Hot spring");
        bouquet1.addFlower(new Rose("Rose", "red", 40));
        bouquet1.addFlower(new Lily("Lily", "pink", 35));
        bouquet1.addFlower(new Tulip("Tulip", "yellow", 25));
        bouquet1.addFlower(new Lily("Lily", "pink", 35));

        bouquet1.toString();

    }
}
