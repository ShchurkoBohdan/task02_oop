import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BouquetManager {
    private List<Bouquet> bouquets;

    public BouquetManager() {
        this.bouquets = new ArrayList<Bouquet>();
    }

    public List<Bouquet> searchByName(String name){
        ArrayList<Bouquet> tempList = new ArrayList<Bouquet>();
        for (Bouquet i : bouquets){
            if (i.getName().equals(name)){
                tempList.add(i);
            }
        }
        return tempList;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public boolean addBouquet(Bouquet bouquet){
        this.bouquets.add(bouquet);
        return true;
    }
}
