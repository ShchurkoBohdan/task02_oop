public class Rose extends Flower {
    private final double price = 25.75;

    Rose(String name, String color, double length){
        super(name, color, length);
    }

    public double getPrice() {
        return price;
    }

    public void smell(){
        System.out.println(" This flower smell as a rose.");
    }
}
