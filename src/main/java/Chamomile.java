public class Chamomile extends Flower{
    private final double price = 10.0;

    public Chamomile(String name, String color, double length) {
        super(name, color, length);
    }

    public double getPrice() {
        return price;
    }

    public void smell(){
        System.out.println(" This flower smell as a chamomile.");
    }
}
