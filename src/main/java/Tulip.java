public class Tulip extends Flower{
    private final double price = 25.35;

    public Tulip(String name, String color, double length) {
        super(name, color, length);
    }
    public double getPrice() {
        return price;
    }
    public void smell(){
        System.out.println(" This flower smell as a tulip.");
    }
}
